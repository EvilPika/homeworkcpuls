#include "List.h"

namespace alg {
	List::List() {
		_head = NULL;
	}
	List::~List() {
		erase();
	}
	void List::print() const {
		//���������
		std::cout << std::endl;
		Node_t* tmp = _head;
		bool flag = false;
		while (tmp != NULL) {
			if (tmp->itemElem != 0 && flag == false) {
				for (int i = 0; i < tmp->itemElem; i++) {
					std::cout << 0 << " --- " << i << std::endl;
					flag = true;
				}
			}
			if (tmp == back()) {
				std::cout << tmp->val << " --- " << tmp->itemElem << std::endl;
				break;
			}
			else{
				std::cout << tmp->val << " --- " << tmp->itemElem << std::endl;
				flag = true;
			}
			if (tmp->next->itemElem != tmp->itemElem + 1) {
				for (int i = tmp->itemElem + 1; i < tmp->next->itemElem; i++) {
					std::cout << 0 << " --- " << i << std::endl;
				}
			}
			tmp = tmp->next;
		}
		std::cout << "###-- " /*<< size() + 1*/  << " --###" << std::endl;
	}
	void List::push_back(double elem) {
		//���������
		int itemC = 0;
		if (_head == NULL) {
			_head = new Node_t(elem, 0);
			return;
		}
		Node_t* tmp = _head;
		while (tmp->next != NULL) {
			tmp = tmp->next;
		}
		if (tmp->val == 0) {
			tmp->val = elem;
			tmp->itemElem = tmp->itemElem + 1;
		}
		else if(tmp->val != 0){
			itemC = tmp->itemElem;
			tmp->next = new Node_t(elem, itemC + 1);
		}

	}
	Node_t* List::front() const {
		//������ � 1 ��-��
		return _head;
	}
	Node_t* List::back() const {
		//������� � ����������
		Node_t* last = _head;
		if (last != NULL) {
			while (last->next != NULL) {
				last = last->next;
			}
		}
		return last;
	}
	bool List::empty() const {
		//�������� �� �������
		return _head == NULL;
	}
	int List::size() const {
		//����� ���������
		//���������

		return back()->itemElem;
	}
	void List::clear() {
		//��������� ���� ��������� 0
		Node_t* tmp = _head;
		while (tmp != NULL) {
			tmp->val = 0;
			tmp = tmp->next;
		}
	}
	void List::erase(){
		Node_t* tmp1;
		if (_head != NULL)
		{
			tmp1 = _head;
			while (tmp1 != NULL)
			{
				tmp1 = _head->next;
				_head = NULL;
				_head = tmp1;
			}
		}
	}
	Node_t* List::at(int atElem) {
		//������� �� �������
		Node_t* post = _head;
		int count = 0;
		bool flag = true;
		if (post != NULL) {
			if (atElem < size() && atElem >= 0) {
				while (post->next != NULL && flag != false) {
					if (count == atElem) {
						return post;
						flag = false;
					}
					count++;
					post = post->next;
				}
				return post;
			}
			else {
				std::cout << "Out of bounds" << std::endl;
				return NULL;
			}
		}
		return NULL;
	}
	int List::find(int findElem) const {
		//����� ��-��
		//���������

		Node_t* post = _head;
		int count = 0;
		bool flag = true;
		if (post != NULL) {
			while (post->next != NULL && flag != false) {
				if (findElem == 0 && post->itemElem != 0 && post == front() || findElem == post->val && post == front()) {
					return count;
					break;
				}
				else if (post->next != back() && post->next->val == findElem) {
					if(post->itemElem + 1 != post->next->itemElem){
						for (int i = post->itemElem; i < post->next->itemElem; i++) {
							count++;
						}
					}
					return count+1;
					break;
				}
				else if (findElem == 0 && post->itemElem + 1 != post->next->itemElem) {
					return count+1;
					break;
				}
				else if (post->next == back() && post->next->val == findElem) {
					return count + 1;
					break;
				}
				count++;
				post = post->next;
			}
			return -666;
		}
		return -666;
	}
	void List::insert(int valElem, int countElem) {
		//�������� ��-� � ��������� �������
		//���������
		Node_t* post = _head;
		int count = 0;
		if (post != NULL) {
			while (post != NULL) {
				count++;
				if (countElem <= 0 && valElem !=0) {
					post = new Node_t(_head->val,_head->itemElem + 1, _head->next);
					_head->val = valElem;
					_head->itemElem = 0;
					_head->next = post;
					for (post != NULL; post = post->next;) {
						post->itemElem = post->itemElem + 1;
					}
					return;
				}
				else if (countElem <= 0 && valElem == 0) {
					post->itemElem = post->itemElem + 1;
					return;
				}
				else if (count == countElem) {
					post->next = new Node_t(valElem,post->itemElem, post->next);
					for (post != NULL; post = post->next;) {
						post->itemElem = post->itemElem + 1;
					}
					return;
				}
				else {
					post = post->next;
				}
			}
			this->push_back(valElem);
		}
		else {
			_head = new Node_t(valElem,0);
		}

	}
	void List::emplace(List* section, int pos){
		Node_t* nodes;
		Node_t* post = _head;
		int saveSize = section->size() + 1;
		if (this->_head == NULL){
			_head = section->_head;
		}
		else if (this->size() < pos){
			saveSize = size() + 1;
			post = section->front();
			while (post->next != NULL) {
				post->itemElem = post->itemElem + saveSize;
				post = post->next;
			}
			post->itemElem = post->itemElem + saveSize;
			this->back()->next = section->_head;
			return;
		} 
		else if (pos <= 0){
			nodes = section->back();
			while (post->next != NULL) {
				post->itemElem = post->itemElem + saveSize;
				//std::cout << post->val << "----" << post->itemElem << std::endl;
				post = post->next;
			}
			post->itemElem = post->itemElem + saveSize;
			nodes->next =_head;
			_head = section->_head;
		}
		else{ 
			Node_t* post2 = _head;
			nodes = this->at(pos-1);
			post = section->front();
			saveSize = section->size() + 1;
			post2 = this->at(pos);

			while (post != NULL) {
				post->itemElem = post->itemElem + nodes->itemElem + 1;
				//std::cout << post->val << "----" << post->itemElem << std::endl;
				post = post->next;
			}
			nodes->next = section->front();

			while (post2 != NULL) {
				//std::cout << post2->val << "----" << post2->itemElem << std::endl;
				push_back(post2->val);
				post2 = post2->next;
			}
		}
	}

//	List List::operator+(List addend){
//		Node_t* tmp1 = _head;
//		Node_t* tmp2 = addend._head;
//		int i = 0;
//		int n = this->size();
//#pragma omp parallel
//		{
//#pragma omp for
//			for (i = 0; i < n; i++)
//			{
//				tmp1->val += tmp2->val;
//				tmp1 = tmp1->next;
//				tmp2 = tmp2->next;
//			}
//		}
//		return *this;
//	}
//	void List::operator=(List assignable){
//
//		this->erase();
//		this->emplace(&assignable, 0);
//
//	}
//
//

}