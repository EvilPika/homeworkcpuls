#pragma once
#include <iostream>;
namespace alg {
	struct Node_t
	{
		Node_t(double elem, int itemElemAdd, Node_t* nextElem = NULL) {
			val = elem;
			next = nextElem;
			itemElem = itemElemAdd;
			prevNul;
		}
		double val = NULL;
		int col = NULL;
		int itemElem = 0;
		int prevNul = 0;
		Node_t* next = NULL;
	};
	struct List
	{
	private:
		Node_t* _head = NULL;

	public:
		List();
		virtual ~List();
		virtual void print() const;
		virtual void push_back(double elem);
		Node_t* front() const;
		Node_t* back() const;
		virtual bool empty() const;
		virtual int size() const;
		virtual void clear();
		virtual void erase();
		Node_t* at(int atElem);
		virtual int find(int findElem) const;
		virtual void insert(int valElem, int countElem);
		virtual void emplace(List* section, int pos);
		//List operator+(List addend);
		//void operator=(List assignable);
	};
}