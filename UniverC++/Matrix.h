#pragma once
#include"List.h"
#include<fstream>
#include<fstream>
#include<string>

namespace alg {
	class Matrix {
	private:
		int _rows = 0, _cols = 0;
	public:
		alg::List **arrList = NULL;
		Matrix(int inRows, int inCols);
		virtual ~Matrix();
		void inputBackElem(int getIndex, int inputBackElem);
		void incertElemMatrix(int inIndex, int inCountElem, int inputElem);
		void inpupFormFileMatrix(std::string inputPut);
		void getColsAndRows() const;
		int charToInt(char elem);
		void printMatrix();
	};
}

