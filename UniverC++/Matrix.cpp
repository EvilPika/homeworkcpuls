#include "Matrix.h"
#include <typeinfo>


namespace alg {
	Matrix::Matrix(int inCols = 0, int inRows = 0)
	{
		_cols = inCols; _rows = inRows;
		arrList = new alg::List*[inRows];
		for (int i = 0; i < inRows; i++)
			arrList[i] = new alg::List();
	}
	void Matrix::getColsAndRows() const {
		std::cout << "Cols -> " << _cols << " Rows -> " << _rows << std::endl;
	}
	void Matrix::inputBackElem(int indexRows, int inputBackElem) {
		if (indexRows < _rows)
			arrList[indexRows]->push_back(inputBackElem);
		else
			std::cout << "Out of index rows in metod inputBackElem" << std::endl;
	}
	void Matrix::incertElemMatrix(int inIndex, int inCountElem, int inputElem) {
		if (inIndex < _rows)
			arrList[inIndex]->insert(inputElem, inCountElem);
		else
			std::cout << "Out of index rows in metod incertElemMatrix" << std::endl;
	}
	void Matrix::printMatrix() {
		for (int i = 0; i < _cols; i++)
			arrList[i]->print();
	}
	int Matrix::charToInt(char elem) {
		return (int)elem - (int)48;
	}
	void Matrix::inpupFormFileMatrix(std::string inputPut) {
		/*
		��������� �������� ��������� �� ����� 
		���������� ����� // ���������� �������� // ���������� �� ������� ���������
		C:\\Users\\User\\source\\repos\\PikaHelp\\UniverC++\\mtx.txt
		*/
		std::string line;
		std::ifstream in(inputPut);
		bool firstLine = true;
		int saveRows = 0, lastIndex = 0, allElem = 0, countElemZero = 1, numberElem = 0, i=0;
		if(in.is_open())
			while (getline(in, line)) {
				if (firstLine) {
					/*
					�� ���������� ������� ��������� 
					*/
					_rows = charToInt(line[2]); _cols = charToInt(line[0]);
					arrList = new alg::List*[_rows];
					for (int i = 0; i < _rows; i++) 
						arrList[i] = new alg::List();

					//getColsAndRows();
					allElem = charToInt(line[2]);
					firstLine = false;
				}
				else {
					/*
					���� ��������� ������� ������ ��� ��������� ������� � �������� � ��������� �� 
					���� ���������
					���� ���������� �� �����
					*/
					if (saveRows != charToInt(line[0])) {
						if (charToInt(line[0]) < 0)
							break;
						for (int i = lastIndex; i < allElem; i++)
							inputBackElem(saveRows, 0);
					}

					if (lastIndex + 1 != charToInt(line[2]))
						for(int i = lastIndex; i < charToInt(line[2]); i++)
							inputBackElem(saveRows, 0);

					saveRows = charToInt(line[0]);
					lastIndex = charToInt(line[2]);
					/*
					���� ����� �� 2� �������� �� ������ ������� ���, ����� ��� ���� �� ������ ��������� �����
					�� ��� �� ����� ���� �� ��� �� ��� ���� ���...
					*/
					i = 4;
					do {
						numberElem *= 10;
						numberElem = (numberElem + charToInt(line[i]));
						i++;
					} while (charToInt(line[i]) > 0);

					incertElemMatrix(charToInt(line[0]), charToInt(line[2]), numberElem);
					numberElem = 0;
					//std::cout << "Push elem in stroka " << line[0] << " stolb " << line[2] << " elem " << line[4] << std::endl;
				}

			}
		in.close();
		printMatrix();
	}
	Matrix::~Matrix()
	{
	}
}